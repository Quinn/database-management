> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Quinn Nguyen

### Assignment2 # Requirements:

1. Screen shot of SQL code
2. Screen shot of customer and company table
3. 7 git commands
4. A2 repos link
   ---------------------------------------------------------------------------------------
1. 
*SCREEN SHOT OF ASSIGNMENT 2 SQL CODE* 
![A2-SQLcode1](img/code-a2-1.png)
![A2-SQLcode2](img/code-a2-2.png)

2.  
*SCREEN SHOT OF ASSIGNMENT 2 CUSTOMER and COMPANY TABLE*
![A2 COMPANY TABLE](img/a2-company.png)
![A2 CUSTOMER TABLE](img/a2-customer.png) 

3. 
> #### GIT COMMANDS WITH SHORT DESCRIPTIONS:




4. 
[A3 Bitbucket repos](https://bitbucket.org/qn18/lis3781/src/master/ "Bitbucket repos")







