> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781

## Quinn Nguyen

### MAIN README:

*Course Work Links:*

1. [A1 README.md](a1/A1_readme.md "My A1 README.md file")
    - Picture of ERD diagram
    - Data dictionary for A1

2. [A2 README.md](a2/A2_readme.md "My A2 README.md file")
    - Picture of 2 tables customer and company;
    - Picture of all the code and insert tables for customer and company;
    - Tables should be created and populated in (fsuid cci server) qn18;
    - 7 git command 

3. [A3 README.md](a3/A3_readme.md "My A3 README.md file")
    - Picture of SQL code for commodity, customer, and order;
    - Picture of 3 populated in Oracle
    - Optional code for reports

4. [A4 README.md](a4/A4_readme.md "My A4 README.md file")
 - Picture of populate tables in MS SQL Server
 - Picture of ERD in MS SQL Server

5. [A5 README.md](a5/A5_readme.md "My A5 README.md file")
 - Continued of A4
 - Picture of populate tables in MS SQL Server
 - Picture of ERD in MS SQL Server
6. [P1 README.md](p1/P1_readme.md "My P1 README.md file")

7. [P2 README.md](p2/P2_readme.md "My P2 README.md file")