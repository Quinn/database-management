> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Quinn Nguyen

### Assignment #4  Requirements:

1. Log into SQL Server
2. Screen shot of SQL code
3. Populate tables using T-SQL
4. ERD must be populated MS SQL server
5. A4 repos link
   --------------------------------------------------------------------------
. 
3. 
*POPULATE TABLES*

![A4 INSERT TABLES 1](img/a4tb1.png)
![A4 INSERT TABLES 2](img/a4tb2.png)
![A4 INSERT TABLES 3](img/a4tb3.png)

4. 
*ERD TABLES A4*
![A4 ERD 1](img/a4erd1.png)
![A4 ERD 2](img/a4erd2.png)

5. 
[A4 Bitbucket repos](https://bitbucket.org/qn18/lis3781/src/master/ "Bitbucket repos")







