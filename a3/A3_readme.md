> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Quinn Nguyen

### Assignment #3  Requirements:

1. Screen shot of SQL code
2. Screen shot of 3 tables in ORACLE
3. Optional: SQL code for the reports
4. A3 repos link
   --------------------------------------------------------------------------
1. 
*SCREEN SHOT OF ASSIGNMENT 3 SQL CODE* 
![A3 SQLcode1](img/a3c1.png)
![A3 SQLcode2](img/a3c2.png)
![A3 SQL INSERT code](img/a3c3.png)
2.  
*SCREEN SHOT OF ASSIGNMENT 3 CUSTOMER, COMMODITY, & ORDER TABLE*
![A3 CUSTOMER,COMMODITY,ORDER TABLE](img/a3table.png)

3. 
*REPORT CODE*
![A3 SQLreport](img/a3rcode.png)
*REPORT TABLE PICTURES*
![A3 SQL report 1 2 3](img/a3r123.png)
![A3 SQL report4 5](img/a3r45.png)
![A3 SQL report 5 6](img/a3r56.png)
![A3 SQL report 7](img/a3r7.png)
![A3 SQL report 8 9 10](img/a3r890.png)

4. 
[A3 Bitbucket repos](https://bitbucket.org/qn18/lis3781/src/master/ "Bitbucket repos")







