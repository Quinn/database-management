> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Quinn Nguyen

### Assignment2 # Requirements:

1. Screen shot of SQL code
2. Screen shot of customer and company table
3. 7 git commands
4. A2 repos link
   ---------------------------------------------------------------------------------------
1. 
*SCREEN SHOT OF ASSIGNMENT 2 SQL CODE* 
![A2-SQLcode1](img/code-a2-1.png)
![A2-SQLcode2](img/code-a2-2.png)

2.  
*SCREEN SHOT OF ASSIGNMENT 2 CUSTOMER and COMPANY TABLE*
![A2 COMPANY TABLE](img/a2-company.png)
![A2 CUSTOMER TABLE](img/a2-customer.png) 

3. 
> #### GIT COMMANDS WITH SHORT DESCRIPTIONS:

1. git init - create a new local repository
2. git status - List the files you've changed and those you still need to add or commit:
3. git add - add file
4. git commit -m - Commit changes to head (but not yet to the remote repository)
5. git push - Send changes to the master branch of your remote repository:
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git remote -v - List all currently configured remote repositories



4. 
[A2 Bitbucket repos](https://bitbucket.org/qn18/lis3781/src/master/ "Bitbucket repos")







