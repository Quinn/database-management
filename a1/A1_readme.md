> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Quinn Nguyen

### Assignment1 # Requirements:
1. Screenshot of A1 ERD
2. Screenshot of Data Dictionary
3. Screenshot of AMPPS
4. Provide git command
5. Bitbucket repos link
  -----------------------------------------------
1. 
*Screen shot a1-erd* 
![A1-ERD](img/erd-a1.png)


2.  
*Screen shot data dictionary*
![A1 data dictionary](img/a1-dataa.png)
![A1 data dictionary](img/a1-data.png) 

3. 
*SCREENSHOT OF AMPPS*
![AMPPS INSTALL](img/AMPPS.png)

4. 
> #### Git commands w/short descriptions:

1. git init - create a new local repository
2. git status - List the files you've changed and those you still need to add or commit:
3. git add - add file
4. git commit -m - Commit changes to head (but not yet to the remote repository)
5. git push - Send changes to the master branch of your remote repository:
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git remote -v - List all currently configured remote repositories



4. 
[A1 Bitbucket repos](https://bitbucket.org/qn18/lis3781/src/master/ "Bitbucket repos")







